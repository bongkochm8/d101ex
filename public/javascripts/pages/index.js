let map;
const polygons = [];
let infoWindow;

$(document).ready(function () {
  console.log("ready");
  getOrders((err, resp) => {
    if (err) {
      toastr["error"]("Error get Order");
    } else {
      renderMarker(resp);
      // console.log("resp: ", resp);
    }
  });

  getRegions((err, resp) => {
    if (err) {
      toastr["error"]("Error get Region");
    } else {
      renderPolygon(resp);
    }
  });
});

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 14.589, lng: 100.4514 },
    zoom: 5,
  });
  infoWindow = new google.maps.InfoWindow();
}

function getOrders(cb) {
  axios
    .get("/api/map/order")
    .then(function (response) {
      // console.log(response);
      cb(null, response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
      cb(error, null);
    });
}

function getRegions(cb) {
  axios
    .get("/api/map/region")
    .then(function (response) {
      // console.log(response);
      cb(null, response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
      cb(error, null);
    });
}

function renderMarker(orders) {
  orders.map((item) => {
    new google.maps.Marker({
      position: { lat: item.latitude, lng: item.longitude },
      map,
      title: `Total: ${item.total}`,
    });
  });
}

function renderPolygon(regions) {
  console.log("regions: ", regions);
  regions.map((item) => {
    const coords = item.geometry.coordinates[0];
    const coordsTransform = coords.map((coordItem) => {
      return { lat: coordItem[1], lng: coordItem[0] };
    });
    // console.log("coords: ", coords);
    // console.log("coordsTransform: ", coordsTransform);
    const p = new google.maps.Polygon({
      paths: coordsTransform,
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
    });
    p.setMap(map);
    addListenersOnPolygon(p, item);
  });
}

const addListenersOnPolygon = function (polygon, region) {
  polygons[polygon] = region;
  google.maps.event.addListener(polygon, "click", function (event) {
    // alert(polygon.indexID);
    console.log("region: ", polygons[polygon]);
    const _region = polygons[polygon];
    renderPopup(event.latLng, _region);
  });
};

function renderPopup(latLng, region) {
  getCountOrder(region.id, (err, result) => {
    if (err) {
      toastr["error"]("Error get Order Count");
    } else {
      console.log(result);
    }
  });
  let contentString =
    "<b> sub district: " +
    region.sub_district_name +
    "</b><br>" +
    "<b> district: " +
    region.district_name +
    "</b><br>" +
    "<b> province: " +
    region.province_name +
    "</b><br><br>" +
    "<b> Count Order </b><br>" +
    "<b>sub district:</b><br>" +
    "<b>district:</b><br>" +
    "<b>province:</b><br><br>" +
    "<b> Sales amount </b><br>" +
    "<b>sub district:</b><br>" +
    "<b>district:</b><br>" +
    "<b>province:</b><br>";

  infoWindow.setContent(contentString);
  infoWindow.setPosition(latLng);
  infoWindow.open(map);
}

function getCountOrder(regionId, cb) {
  axios
    .get(`/api/map/getCount/${regionId}`)
    .then(function (response) {
      // console.log(response);
      cb(null, response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
      cb(error, null);
    });
}

function getSalesAmount(regionId) {}
