$(document).ready(function () {
  // console.log("ready");
  $("#btnUpload").click(function (e) {
    // e.preventDefault();
    let myForm = document.getElementById("formUpload");
    var formData = new FormData(myForm);
    $("#btnUpload").attr("disabled");
    $("#btnUpload>label").html("Loading...");
    $("#btnUpload>span").removeClass("d-none");

    $.ajax({
      url: "/api/map/region/upload",
      type: "POST",
      data: formData,
      success: function (data) {
        $("#btnUpload").removeAttr("disabled");
        $("#btnUpload>label").html("Upload");
        $("#btnUpload>span").addClass("d-none");
        if (data.success === true) {
          toastr["success"]("Upload Success");
        } else {
          toastr["error"]("Upload UnSuccess");
        }
      },
      cache: false,
      contentType: false,
      processData: false,
    });
  });
});
