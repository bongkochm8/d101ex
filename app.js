var createError = require("http-errors");
var express = require("express");
const bodyParser = require("body-parser");
const compress = require("compression");
const cors = require("cors");
const methodOverride = require("method-override");
const morgan = require("morgan");
const helmet = require("helmet");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var multer = require("multer");

var indexRouter = require("./routes/index");
// var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.use(
  "/css",
  express.static(__dirname + "/node_modules/bootstrap/dist/css/")
);
app.use("/js", express.static(__dirname + "/node_modules/bootstrap/dist/js/"));
app.use("/axios", express.static(__dirname + "/node_modules/axios/dist/"));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// request logging. dev: console | production: file
app.use(morgan("dev"));

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// gzip compression
app.use(compress());

// lets you use HTTP verbs such as PUT or DELETE
// in places where the client doesn't support it
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// app.use(multer({ dest: "./uploads/" }).single("file"));

app.use(
  multer({
    dest: __dirname + "/uploads/",
    rename: function (fieldname, filename) {
      return Date.now();
    },
  }).single("file")
);

app.use("/", indexRouter);
// app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  console.log(err);
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err);
  // res.render("error");
});

module.exports = app;
