var express = require("express");
var router = express.Router();
const apiRoutes = require("./api/map.route");
const webRoutes = require("./web/map.route");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.use("/api", apiRoutes);
router.use("/", webRoutes);

module.exports = router;
