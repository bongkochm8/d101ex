var express = require("express");
var router = express.Router();

const mapController = require("../../controllers/map.controller");

// url post /api/map/region/upload
router.post("/map/region/upload", mapController.uploadRegion);
// url get /api/map/order
router.get("/map/order", mapController.listOrder);

// unfinished
// url get /api/map/region
router.get("/map/region", mapController.listRegion);

// unfinished
router.get("/map/getCount/:regionId", mapController.getCount);
// unfinished
router.get("/map/getAmount/:regionId", mapController.getAmount);

module.exports = router;
