var express = require("express");
var router = express.Router();

//url get /map
router.get("/map", (req, res) => {
  console.log("map");
  res.render("index");
});

router.get("/region/upload", (req, res) => {
  res.render("upload");
});

module.exports = router;
