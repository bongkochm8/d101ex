"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Region extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Region.init(
    {
      style: DataTypes.JSONB,
      area_id: DataTypes.STRING,
      province_id: DataTypes.STRING,
      district_id: DataTypes.STRING,
      sub_district_id: DataTypes.STRING,
      province_name: DataTypes.STRING,
      province_name_th: DataTypes.STRING,
      district_name: DataTypes.STRING,
      district_name_th: DataTypes.STRING,
      sub_district_name: DataTypes.STRING,
      sub_district_name_th: DataTypes.STRING,
      postcode: DataTypes.STRING,
      multiGeometry: DataTypes.JSONB,
      geometry: { type: DataTypes.GEOGRAPHY("POLYGON"), allowNull: false },
    },
    {
      sequelize,
      modelName: "Region",
    }
  );
  return Region;
};
