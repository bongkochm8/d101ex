"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Order.init(
    {
      latitude: { type: DataTypes.REAL, allowNull: false },
      longitude: { type: DataTypes.REAL, allowNull: false },
      total: { type: DataTypes.REAL, allowNull: false },
      geometry: { type: DataTypes.GEOGRAPHY("POINT"), allowNull: false },
    },
    {
      sequelize,
      modelName: "Order",
    }
  );
  return Order;
};
