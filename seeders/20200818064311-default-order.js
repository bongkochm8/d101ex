"use strict";
const fs = require("fs");
var path = require("path");
const CSVToJSON = require("csvtojson");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      const orders = await CSVToJSON().fromFile(
        path.join(__dirname, "../data/orders_data.csv")
      );
      const ordersTransform = orders.map((item) => {
        return {
          latitude: parseFloat(item.lat),
          longitude: parseFloat(item.lng),
          total: parseFloat(item.total),
          geometry: Sequelize.fn(
            "ST_GeomFromText",
            `POINT(${parseFloat(item.lng)} ${parseFloat(item.lat)})`
          ),
          createdAt: new Date(),
          updatedAt: new Date(),
        };
      });

      return await queryInterface.bulkInsert("Orders", ordersTransform, {});
    } catch (err) {
      console.log(err);
    }

    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
