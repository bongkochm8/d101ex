var parser = require("xml2js");
const fs = require("fs");
var path = require("path");
const CSVToJSON = require("csvtojson");
//db
const Order = require("../models").Order;
const Region = require("../models").Region;

exports.uploadRegion = async (req, res) => {
  if (!req.file) return res.json({ success: false, error: "file invalid" });
  let data = fs.readFileSync(req.file.path, "utf8");
  parser.parseString(
    data,
    {
      explicitArray: false,
      explicitCharkey: false,
      explicitChildren: false,
      ignoreAttrs: false,
      mergeAttrs: true,
      charkey: "value",
    },
    function (err, result) {
      const placemark = result.kml.Document.Folder.Placemark;
      const dataTransform = placemark.map((item) => {
        const simpleData = item.ExtendedData.SchemaData.SimpleData;
        const simpleDataTransform = {};
        simpleData.map((simpleDataItem) => {
          simpleDataTransform[simpleDataItem.name] = simpleDataItem.value;
        });
        const coordinates =
          item.MultiGeometry.Polygon.outerBoundaryIs.LinearRing.coordinates;
        const coordinatesTransform = coordinates.split(" ").map((item) => {
          const sp = item.split(",");
          return [sp[0], sp[1]];
        });
        const polygon = {
          type: "Polygon",
          coordinates: [coordinatesTransform],
        };
        return {
          style: item.Style,
          area_id: simpleDataTransform.area_id,
          province_id: simpleDataTransform.province_id,
          district_id: simpleDataTransform.district_id,
          sub_district_id: simpleDataTransform.sub_district_id,
          province_name: simpleDataTransform.province_name,
          province_name_th: simpleDataTransform.province_name_th,
          district_name: simpleDataTransform.district_name,
          district_name_th: simpleDataTransform.district_name_th,
          sub_district_name: simpleDataTransform.sub_district_name,
          sub_district_name_th: simpleDataTransform.sub_district_name_th,
          postcode: simpleDataTransform.postcode,
          // multiGeometry: item.MultiGeometry,
          geometry: polygon,
          createdAt: new Date(),
          updatedAt: new Date(),
        };
      });

      Promise.all(
        dataTransform.map(async (region) => {
          return await Region.create(region);
        })
      )
        .then((result) => {
          res.json({ success: true });
        })
        .catch((err) => {
          res.json({ success: false, error: err });
        });

      // Region.bulkCreate(dataTransform)
      //   .then((result) => {
      //     res.json({ success: true });
      //   })
      //   .catch((err) => {
      //     res.json({ success: false, error: err });
      //   });
    }
  );
};

exports.listOrder = async (req, res) => {
  const orders = await Order.findAll();
  res.json(orders);
};

// unfinished
exports.listRegion = async (req, res) => {
  const regions = await Region.findAll({ limit: 1000 });
  res.json(regions);
};

// unfinished
exports.getCount = async (req, res) => {
  const regions = await Region.findByPk(req.params.regionId);
  const province = await Region.findAll({
    where: { province_id: regions.province_id },
  });

  res.json(regions);
};

// unfinished
exports.getAmount = async (req, res) => {
  res.json("getAmount");
};
