var parser = require("xml2js");
const fs = require("fs");
var path = require("path");
const CSVToJSON = require("csvtojson");
//db
const Order = require("../models").Order;
const Region = require("../models").Region;

exports.upload2 = async (req, res) => {
  if (!req.file) return res.json({ success: false, error: "file invalid" });
  let data = fs.readFileSync(req.file.path, "utf8");
  parser.parseString(
    data,
    {
      explicitArray: false,
      explicitCharkey: false,
      explicitChildren: false,
      ignoreAttrs: false,
      mergeAttrs: true,
      charkey: "value",
    },
    function (err, result) {
      const placemark = result.kml.Document.Folder.Placemark;
      const dataTransform = placemark.map((item) => {
        const simpleData = item.ExtendedData.SchemaData.SimpleData;
        const simpleDataTransform = {};
        simpleData.map((simpleDataItem) => {
          simpleDataTransform[simpleDataItem.name] = simpleDataItem.value;
        });
        const coordinates =
          item.MultiGeometry.Polygon.outerBoundaryIs.LinearRing.coordinates;
        const coordinatesTransform = coordinates.split(" ").map((item) => {
          const sp = item.split(",");
          return [sp[0], sp[1]];
        });
        const polygon = {
          type: "Polygon",
          coordinates: [coordinatesTransform],
        };
        return {
          style: item.Style,
          area_id: simpleDataTransform.area_id,
          province_id: simpleDataTransform.province_id,
          district_id: simpleDataTransform.district_id,
          sub_district_id: simpleDataTransform.sub_district_id,
          province_name: simpleDataTransform.province_name,
          province_name_th: simpleDataTransform.province_name_th,
          district_name: simpleDataTransform.district_name,
          district_name_th: simpleDataTransform.district_name_th,
          sub_district_name: simpleDataTransform.sub_district_name,
          sub_district_name_th: simpleDataTransform.sub_district_name_th,
          postcode: simpleDataTransform.postcode,
          multiGeometry: item.MultiGeometry,
          geometry: polygon,
          createdAt: new Date(),
          updatedAt: new Date(),
        };
      });

      Region.bulkCreate(dataTransform).then((result) => {
        res.json({ success: true });
      });
      // Promise.all(
      //   dataTransform.map(async (item) => {
      //     try {
      //       await Region.create(item);
      //     } catch (error) {
      //       console.log("create region error: ", error);
      //     }
      //   })
      // );
    }
  );
  // res.json(req.file);
};

exports.upload = async (req, res) => {
  const xml = fs.readFileSync(path.join(__dirname, "../data/example.kml"));
  parser.parseString(
    xml,
    {
      explicitArray: false,
      explicitCharkey: false,
      explicitChildren: false,
      ignoreAttrs: false,
      mergeAttrs: true,
      charkey: "value",
    },
    function (err, result) {
      // console.log(result.root);
      res.json(result);
    }
  );
};

exports.orderFile = async (req, res) => {
  // const csv = fs.readFileSync(path.join(__dirname, "../data/orders_data.csv"));
  try {
    const order = await CSVToJSON().fromFile(
      path.join(__dirname, "../data/orders_data.csv")
    );
    const orderTransform = order.map((item) => {
      return {
        latitude: item.lat,
        longitude: item.lng,
        total: item.total,
        geometry: { type: "Point", coordinates: [item.lat, item.lng] },
      };
    });

    res.json(orderTransform);
    // log the JSON array
    // console.log(users);
  } catch (err) {
    console.log(err);
  }
};

exports.create = async (req, res) => {
  const point = { type: "Point", coordinates: [39.807222, -76.984722] };
  const order = await Order.create({
    latitude: 39.807222,
    longitude: -76.984722,
    total: 439,
    geometry: point,
  });
  res.json(order);
  // res.json({ success: true });
};
