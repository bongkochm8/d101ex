"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Regions", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      style: Sequelize.JSONB,
      area_id: Sequelize.STRING,
      province_id: Sequelize.STRING,
      district_id: Sequelize.STRING,
      sub_district_id: Sequelize.STRING,
      province_name: Sequelize.STRING,
      province_name_th: Sequelize.STRING,
      district_name: Sequelize.STRING,
      district_name_th: Sequelize.STRING,
      sub_district_name: Sequelize.STRING,
      sub_district_name_th: Sequelize.STRING,
      postcode: Sequelize.STRING,
      multiGeometry: Sequelize.JSONB,
      geometry: { type: Sequelize.GEOGRAPHY("POLYGON"), allowNull: false },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Regions");
  },
};
